# Analog phone trunking over Cat5/6 
Use standard twisted pair Cat5/6 cabling which is usually used for Ethernet as a trunk for 4 phone lines. 

Running analog phones over Cat5 cables was more common when 100BASE-T was mostly used, because that only utilized 4 of the 8 pins for Ethernet, leaving 4 pins for other use. Gigabit Ethernet uses the full 8 pins. However, as long as the cable is not actually used for Gigabit Ethernet, it can still be used for analog telephony and we can use all 8 pins.

RJ-11 cables can fit in RJ-45 sockets. Because of the ubiquitousness of Ethernet in modern networking, they are usually cheaper to buy at electronics shops. You could also cut the RJ-45 cables and add RJ-11 jacks to them. This pcb gives more flexibility because you can easily swap different cables and keep using them for Ethernet without first having to add the jack back. The intended use case for this pcb is deploying retro telecom infrastructure at hacker camps.

## Trunking cable types
[ANSI/TIA-568 § Wiring](https://en.wikipedia.org/wiki/ANSI/TIA-568#Wiring)

The wiring is specified in the ANSI/TIA-568 standard. This standard specifies two different wirings: T568A and T568B.

* Straight-trough cables use either T568A or T568B on both sides
* Partial (2 pair) crossover, T568A on one end and T568B on the other
* Full (4 pair) crossover, see [Ethernet crossover cable § Fully crossed](https://en.wikipedia.org/wiki/Ethernet_crossover_cable#Fully_crossed)

Since multiple types of cables can be used, we do not know the order in which the phone line outputs are put, without taking a careful look at the cable. The board contains a warning about this.

## Board connectors
* 1x RJ45 jack for the trunk 
* 4x RJ45 jack to either the FXO or FXS side 
* 1x RJ45 so the boards can be daisy chained
* 4x switch to enable/disable passtrough per port (to prevent accidental interference downstream)
* Punch block as a backup in case no cat5/6 cables are available 

## Building 
* TODO instructions for ordering

## Credits
Made by Eloy Degen, intially for [BornHack 2024](https://bornhack.dk/bornhack-2024/). Licensed under the [BSD Zero Clause License](./LICENSE).

## External links
* [FXO and FXS](https://osmocom.org/projects/retronetworking/wiki/FXO_and_FXS) (Retronetworking wiki)
* [TIA-568.0-E](https://web.archive.org/web/20221222063425/https://imlive.s3.amazonaws.com/Federal%20Government/ID62048956821926688123123361973875756393/Attachment%201i%20TIA-568%20Telecomm%20Cabling%20Standard.pdf)
* [Picture: CC BY 2.0, Benny Mazur](https://flickr.com/photos/benimoto/649258129/)

![Trunk cat](./media/cattrunk.jpg)

Trunk cat :)
